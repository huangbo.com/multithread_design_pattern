package future009;

import java.util.concurrent.Callable;

public class Host {
    // 使用Callable
    public Data request(final int count, final char c) {
        System.out.println("    requst(" + count + ", " + c + ") BEGIN");
        // 1.创建FutureData的实例
        // (向构造函数中传递Callable<RealData>)
        FutureData future = new FutureData(new Callable<RealData>() {
            @Override
            public RealData call() throws Exception {
                return new RealData(count, c);
            }
        });
        // 启动一个线程，用于创建RealData的实例
        new Thread(future).start();
        System.out.println("    requst(" + count + ", " + c + ") END");
        return future;
    }
    // 常规写法
    /*public Data request(final int count, final char c) {
        System.out.println("    requst(" + count + ", " + c + ") BEGIN");
        // (1)创建FutureData的实例
        final FutureData future = new FutureData();
        // (2)启动一个新线程，用于创建RealData的实例
        new Thread(() -> {
            RealData realData = new RealData(count, c);
            future.setRealData(realData);
        }).start();
        System.out.println("    requst(" + count + ", " + c + ") END");
        return future;
    }*/
}
