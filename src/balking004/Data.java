package balking004;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Data {
    /**
     * 保存的文件名称
     */
    private final String filename;
    /**
     * 数据内容
     */
    private String content;
    /**
     * 修改后的内容若未保存，则为true
     */
    private boolean changed;

    /**
     * 构造方法
     * @param filename 文件名称
     * @param content 文件内容
     */
    public Data(String filename, String content) {
        this.filename = filename;
        this.content = content;
        this.changed = true;
    }

    /**
     * 修改数据内容
     * @param newContent
     */
    public synchronized void change(String newContent) {
        content = newContent;
        changed = true;
    }

    /**
     * 保存方法
     * @throws IOException
     */
    public synchronized  void save() throws IOException {
        if (!changed) {
            return;
        }
        doSave();
        changed = false;
    }

    /**
     * 讲数据内容实际保存到文件中
     * @throws IOException
     */
    private void doSave() throws IOException {
        System.out.println(Thread.currentThread().getName() + " calls doSave, content = " + content);
        Writer writer = new FileWriter(filename);
        writer.write(content);
        writer.close();
    }
}
