package activeobject012.activeobject;

public abstract class Result<T> {
    public abstract T getResultValue();
}
