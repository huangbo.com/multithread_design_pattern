package activeobject012;

import activeobject012.activeobject.ActiveObject;
import activeobject012.activeobject.Result;

public class MarkerClientThread extends Thread {
    private final ActiveObject activeObject;
    private final char fillchar;
    public MarkerClientThread(String name, ActiveObject activeObject) {
        super(name);
        this.activeObject = activeObject;
        this.fillchar = name.charAt(0);
    }

    @Override
    public void run() {
        try {
            for (int i = 0; true; i++) {
                // 有返回值的调用
                Result<String> result = activeObject.makeString(i, fillchar);
                Thread.sleep(10);
                String value = result.getResultValue();
                System.out.println(Thread.currentThread().getName() + ": value = " + value);
            }
        } catch (InterruptedException e) {
        }
    }
}
