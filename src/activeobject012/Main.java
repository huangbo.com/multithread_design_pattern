package activeobject012;

import activeobject012.activeobject.ActiveObject;
import activeobject012.activeobject.ActiveObjectFactory;

public class Main {
    public static void main(String[] args) {
        ActiveObject activeObject = ActiveObjectFactory.createActiveObject();
        new MarkerClientThread("Alice", activeObject).start();
        new MarkerClientThread("Bobby", activeObject).start();
        new DisplayClientThread("Chris", activeObject).start();
    }
}
