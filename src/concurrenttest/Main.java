package concurrenttest;

import java.util.Random;
import java.util.concurrent.Semaphore;

class Log {
    public static void print(String s) {
        System.out.println(Thread.currentThread().getName() + ":" + s);
    }
}

/**
 * 资源个数有限
 */
class BoundedResource {
    private final Semaphore semaphore;
    private final int permits;
    private final static Random random = new Random(314159);

    /**
     * 构造函数
     *
     * @param permits
     */
    public BoundedResource(int permits) {
        this.semaphore = new Semaphore(permits);
        this.permits = permits;
    }

    /**
     * 使用资源
     *
     * @throws InterruptedException
     */
    public void use() throws InterruptedException {
        semaphore.acquire();
        try {
            doUse();
        } finally {
            semaphore.release();
        }
    }

    /**
     * 实际使用资源
     *
     * @throws InterruptedException
     */
    protected void doUse() throws InterruptedException {
        Log.print("BEGIN: used = " + (permits - semaphore.availablePermits()));
        Thread.sleep(random.nextInt(500));
        Log.print("END:   used = " + (permits - semaphore.availablePermits()));
    }
}

/**
 * 使用资源线程
 */
class UserThread extends Thread {
    private final static Random random = new Random(26535);
    private final BoundedResource resource;

    public UserThread(BoundedResource resource) {
        this.resource = resource;
    }

    @Override
    public void run() {
        try {
            resource.use();
            Thread.sleep(random.nextInt(3000));
        } catch (InterruptedException e) {
        }
    }
}

public class Main {
    public static void main(String[] args) {
        // 设置3个资源
        BoundedResource resource = new BoundedResource(3);

        // 10个线程使用资源
        for (int i = 0; i < 10; i++) {
            new UserThread(resource).start();
        }
    }
}
