package singlethreadedexecution001;

public class UserThread extends Thread {
    private final Gate gate;
    private final String myname;
    private final String myaddress;

    /**
     * 构造方法
     * @param gate 门对象
     * @param myname 我的名字
     * @param myaddress 我的地址
     */
    public UserThread(Gate gate, String myname, String myaddress) {
        this.gate = gate;
        this.myname = myname;
        this.myaddress = myaddress;
    }

    public void run() {
        System.out.println(myname + "BEGIN");
        while (true) {
            gate.pass(myname, myaddress);
        }
    }
}
